package facci.citasmascotas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class Medico extends AppCompatActivity {

    private EditText nom_doc, ape_doc, celu_doc, email_doc, consult_doc;
    private Button guardar;
    private ImageView regresar, salir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medico);
        nom_doc = findViewById(R.id.nom_doc);
        ape_doc = findViewById(R.id.ape_doc);
        celu_doc = findViewById(R.id.celu_doc);
        email_doc = findViewById(R.id.email_doc);
        consult_doc = findViewById(R.id.consult_doc);
        guardar = findViewById(R.id.btn4_guardar);
        regresar = findViewById(R.id.regresar);
        salir = findViewById(R.id.salir);


        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Medico.this, Detalle_mascota.class);
                finish();
                startActivity(intent);
            }
        });

        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Medico.this, MainActivity.class);
                finish();
                startActivity(intent);
            }
        });

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Medico.this, EstadoGuardado.class);
                finish();
                startActivity(intent);
            }
        });
    }
}