package facci.citasmascotas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import de.hdodenhof.circleimageview.CircleImageView;

public class Listado_mascotas extends AppCompatActivity {

    private CardView card1, card2;
    private CircleImageView user_icon ;
    private ImageView logout_icon;
    private LinearLayout agregar_mascota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_mascotas);

        card1 = findViewById(R.id.listado_mascotas_card1);
        card2 = findViewById(R.id.listado_mascotas_card2);

        user_icon = findViewById(R.id.listado_mascotas_user_icon);
        logout_icon = findViewById(R.id.listado_mascotas_logout_icon);

        agregar_mascota = findViewById(R.id.listado_mascotas_agregar);

        card1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Listado_mascotas.this, Detalle_mascota.class);
                startActivity(intent);
            }
        });

        card2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Listado_mascotas.this, Detalle_mascota.class);
                startActivity(intent);
            }
        });

        user_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Listado_mascotas.this, editar_perfil.class);
                startActivity(intent);
            }
        });

        logout_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Listado_mascotas.this, MainActivity.class);
                finish();
                startActivity(intent);
            }
        });

        agregar_mascota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Listado_mascotas.this, Agregar_mascota.class);
                startActivity(intent);
            }
        });
    }

}