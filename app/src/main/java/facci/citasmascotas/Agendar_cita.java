package facci.citasmascotas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class Agendar_cita extends AppCompatActivity {

    private EditText hora_cita;
    private EditText fecha_cita;
    private TextView consult_cita, nota_cita;
    private Button btn_guardar;
    private ImageView regresar, salir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agendar_cita);
        hora_cita = findViewById(R.id.hora_cita);
        fecha_cita = findViewById(R.id.fecha_cita);
        consult_cita = findViewById(R.id.consult_cita);
        nota_cita = findViewById(R.id.nota_cita);
        btn_guardar = findViewById(R.id.btn_guardar);
        regresar = findViewById(R.id.regresar);
        salir = findViewById(R.id.salir);

        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Agendar_cita.this, Detalle_mascota.class);
                finish();
                startActivity(intent);
            }
        });

        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Agendar_cita.this, MainActivity.class);
                finish();
                startActivity(intent);
            }
        });

        btn_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Agendar_cita.this, EstadoGuardado.class);
                finish();
                startActivity(intent);

            }
        });
    }
}
