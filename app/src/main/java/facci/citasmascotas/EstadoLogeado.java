package facci.citasmascotas;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class EstadoLogeado extends AppCompatActivity {

    private TextView init;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estado_login);


        init=findViewById(R.id.init);



        init.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EstadoLogeado.this, Listado_mascotas.class);
                finish();
                startActivity(intent);
            }
        });
    }
}
