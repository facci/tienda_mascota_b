package facci.citasmascotas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import de.hdodenhof.circleimageview.CircleImageView;

public class Agregar_mascota extends AppCompatActivity {
    private CircleImageView user_icon ;
    private ImageView logout_icon;
    private LinearLayout agregar_mascota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_mascota);

        user_icon = findViewById(R.id.agregar_mascota_user_icon);
        logout_icon = findViewById(R.id.agregar_mascota_logout_icon);

        agregar_mascota = findViewById(R.id.agregar_mascota_agregar);

        user_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        logout_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Agregar_mascota.this, MainActivity.class);
                finish();
                startActivity(intent);
            }
        });

        agregar_mascota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Agregar_mascota.this, EstadoGuardado.class);
                finish();
                startActivity(intent);
            }
        });

    }
}